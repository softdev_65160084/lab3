/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;


/**
 *
 * @author Siwak
 */
class OX {

    static boolean checkDraw(char[][] table) {
         for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == '-') {
                    return false; // If any empty cell is found, the table is not full
                }
            }
        }
        return true;
    }
    char[][] table;
    char currentPlayer;
    static boolean checkWin(char[][] table, char currentPlayer) {
        if(checkRow(table,currentPlayer)){
            return true;
        }
        if(checkCol(table,currentPlayer)){
            return true;
        }
        return false;
    }
    static boolean checkRow(char[][] table, char currentPlayer){
            for(int row=0;row<3;row++){
                if(table[row][0]==currentPlayer && table[row][1]==currentPlayer && table[row][2]==currentPlayer){
                return true;
            }              
        }
        return false;
    }
    
    static boolean checkCol(char[][] table, char currentPlayer){
            for(int col=0;col<3;col++){
                if(table[0][col]==currentPlayer && table[1][col]==currentPlayer && table[2][col]==currentPlayer){
                return true;
            }              
        }
        return false;
    }
}
