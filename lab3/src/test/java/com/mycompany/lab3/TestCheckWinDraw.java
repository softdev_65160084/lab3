/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Siwak
 */
public class TestCheckWinDraw {
    
    public TestCheckWinDraw() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow1_X_output_true(){
        char[][] table = {{'X','X','X'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow2_X_output_true(){
        char[][] table = {{'-','-','-'},
                          {'X','X','X'},
                          {'-','-','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinRow3_X_output_true(){
        char[][] table = {{'-','-','-'},
                          {'-','-','-'},
                          {'X','X','X'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    
    
    @Test
    public void testCheckWinRow1_X_output_false(){
        char[][] table = {{'X','O','X'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinRow2_X_output_false(){
        char[][] table = {{'-','-','-'},
                          {'X','-','X'},
                          {'-','O','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinRow3_X_output_false(){
        char[][] table = {{'-','-','-'},
                          {'-','-','-'},
                          {'X','X','-'}};
        char player = 'X';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    
//    
//    @Test
//    public void testCheckWinRow1_O_output_true(){
//        char[][] table = {{'O','O','O'},
//                          {'-','-','-'},
//                          {'-','-','-'}};
//        char player = 'O';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinRow2_O_output_true(){
//        char[][] table = {{'-','-','-'},
//                          {'O','O','O'},
//                          {'-','-','-'}};
//        char player = 'O';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinRow3_O_output_true(){
//        char[][] table = {{'-','-','-'},
//                          {'-','-','-'},
//                          {'O','O','O'}};
//        char player = 'O';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
    
    @Test
    public void testCheckWinCol1_O_output_true(){
        char[][] table = {{'O','-','-'},
                          {'O','-','-'},
                          {'O','-','-'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    public void testCheckWinCol2_O_output_true(){
        char[][] table = {{'-','O','-'},
                          {'-','O','-'},
                          {'-','O','-'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    @Test
    public void testCheckWinCol3_O_output_true(){
        char[][] table = {{'-','-','O'},
                          {'-','-','O'},
                          {'-','-','O'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(true, result);
    }
    
    
    @Test
    public void testCheckWinCol1_O_output_false(){
        char[][] table = {{'O','-','-'},
                          {'O','X','-'},
                          {'-','-','-'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    public void testCheckWinCol2_O_output_false(){
        char[][] table = {{'-','-','-'},
                          {'-','O','-'},
                          {'-','O','X'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    @Test
    public void testCheckWinCol3_O_output_false(){
        char[][] table = {{'-','-','X'},
                          {'-','-','O'},
                          {'-','-','O'}};
        char player = 'O';
        boolean result = Lab2_2.checkWin(table, player);
        assertEquals(false, result);
    }
    
//    @Test
//    public void testCheckWinCol1_X_output_true(){
//        char[][] table = {{'X','-','-'},
//                          {'X','-','-'},
//                          {'X','-','-'}};
//        char player = 'X';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinCol2_X_output_true(){
//        char[][] table = {{'-','X','-'},
//                          {'-','X','-'},
//                          {'-','X','-'}};
//        char player = 'X';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    @Test
//    public void testCheckWinCol3_X_output_true(){
//        char[][] table = {{'-','-','X'},
//                          {'-','-','X'},
//                          {'-','-','X'}};
//        char player = 'X';
//        boolean result = Lab2_2.checkWin(table, player);
//        assertEquals(true, result);
//    }
//    
    @Test
    public void testCheckDraw_full_output_true(){
        char[][] table = {{'X','O','X'},
                          {'X','O','X'},
                          {'O','X','O'}};
        boolean result = Lab2_2.checkDraw(table);
        assertEquals(true, result);
    }

    @Test
    public void testCheckDraw_empty_output_false(){
        char[][] table = {{'-','-','-'},
                          {'-','-','-'},
                          {'-','-','-'}};
        boolean result = Lab2_2.checkDraw(table);
        assertEquals(false, result);
    }
    
    
    @Test
    public void testCheckDraw_notFull_output_false(){
        char[][] table = {{'-','-','-'},
                          {'-','X','-'},
                          {'O','-','-'}};
        boolean result = Lab2_2.checkDraw(table);
        assertEquals(false, result);
    }
}
